package xtool.xdepspringboot.tasks

import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import xtool.xdepjava.tasks.JavaTask
import xtool.xdepspringboot.model.Entity
import xtool.xdepspringboot.model.Repository
import xtool.xdepspringboot.model.Rest
import xtool.xdepspringboot.model.SpringBootProject
import java.nio.file.Path
import java.nio.file.Paths

/**
 * Classe de task que realiza a leitura e manipulação de arquivo *.java.
 */
@Component
class SpringBootTask(
    private val environment: Environment,
    private val javaTask: JavaTask
) {

    private val log = LoggerFactory.getLogger(SpringBootTask::class.java)

    /**
     * Retorna o projeto SpringBoot para o caminho especificado.
     */
    fun getProject(path: Path = Paths.get(System.getProperty("user.dir"))): SpringBootProject {
        return SpringBootProject(
            path,
            environment,
            javaTask,
            this
        )
    }

    fun findEntityByName(entityName: String, springBootProject: SpringBootProject = getProject()): Entity? {
        return springBootProject.entities.asSequence().find { it.name == entityName }
    }

    /**
     * Retorna a classe rest para a entidade correspondente.
     */
    fun restForEntity(entity: Entity, springBootProject: SpringBootProject = getProject()): Rest? {
        return springBootProject.rests.asSequence().find { it.name == "${entity.name}Rest" }
    }

    /**
     * Retorna a interface de repository para a entidade correspondente.
     */
    fun repositoryForEntity(entity: Entity, springBootProject: SpringBootProject = getProject()): Repository? {
        return springBootProject.repositories.asSequence().find { it.name == "${entity.name}Repository" }
    }

    fun listViewForEntity(entity: Entity, springBootProject: SpringBootProject = getProject()): Entity? {
        return springBootProject.entities.asSequence().find { it.name == "${entity.name}ListView" }
    }
}
