package xtool.xdepspringboot.command.converter

import org.springframework.stereotype.Component
import picocli.CommandLine.ITypeConverter
import xtool.xdepspringboot.model.Rest
import xtool.xdepspringboot.tasks.SpringBootTask

@Component
class RestConverter(
    private val springBootTask: SpringBootTask,
) : ITypeConverter<Rest> {
    override fun convert(value: String?): Rest {
        val rests = springBootTask.getProject().rests
        return rests.asSequence().find { it.name == value }!!
    }
}
