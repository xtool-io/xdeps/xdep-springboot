package xtool.xdepspringboot.command.converter

import org.jboss.forge.roaster.model.source.JavaClassSource
import org.springframework.stereotype.Component
import picocli.CommandLine.ITypeConverter
import xtool.xdepspringboot.tasks.SpringBootTask

@Component
class EntityConverter(
    private val springBootTask: SpringBootTask,
) : ITypeConverter<JavaClassSource> {
    override fun convert(value: String?): JavaClassSource {
        val entities = springBootTask.getProject().entities
        return entities.asSequence().find { it.name == value }!!
    }
}
