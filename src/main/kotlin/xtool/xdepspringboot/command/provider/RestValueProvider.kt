package xtool.xdepspringboot.command.provider

import org.springframework.stereotype.Component
import xtool.xdepspringboot.tasks.SpringBootTask

@Component
class RestValueProvider(
    private val springBootTask: SpringBootTask,
) : Iterable<String> {
    override fun iterator(): Iterator<String> {
        val rests = springBootTask.getProject().rests
        return rests.asSequence()
            .map { it.name }
            .iterator()
    }
}
