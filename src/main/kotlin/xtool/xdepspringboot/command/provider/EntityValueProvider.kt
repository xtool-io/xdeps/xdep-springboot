package xtool.xdepspringboot.command.provider

import org.springframework.stereotype.Component
import xtool.xdepspringboot.tasks.SpringBootTask

@Component
class EntityValueProvider(
    private val springBootTask: SpringBootTask,
) : Iterable<String> {
    override fun iterator(): Iterator<String> {
        val entities = springBootTask.getProject().entities
        return entities.asSequence()
            .map { it.name }
            .iterator()
    }
}
