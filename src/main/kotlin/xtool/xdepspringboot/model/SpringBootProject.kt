package xtool.xdepspringboot.model

import org.springframework.core.env.Environment
import xtool.xdepjava.tasks.JavaTask
import xtool.xdepspringboot.tasks.SpringBootTask
import java.nio.file.Files
import java.nio.file.Path
import kotlin.io.path.name

data class SpringBootProject(
    private var path: Path,
    private val environment: Environment,
    private val javaTask: JavaTask,
    private val springBootTask: SpringBootTask
) {

    /**
     * Retorna a lista de todas as entidades JPA do projeto. As entidades são pesquisadas tendo como base o
     * diretório definido na variável 'entity.path'.
     */
    val entities: Iterator<Entity> by lazy {
        val entityPath = path.resolve(environment.getRequiredProperty("entity.path"))
        Files.walk(entityPath)
            .filter { it.name.endsWith(".java") }
            .map { javaTask.readClass(it) }
            .filter { it.hasAnnotation("Entity") }
            .map { Entity(it, springBootTask) }
            .iterator()
    }

    /**
     * Retorna a lista de todos as interfaces de Repository do projeto.
     */
    val repositories: Iterator<Repository> by lazy {
        val repositoryPath = path.resolve(environment.getRequiredProperty("repository.path"))
        Files.walk(repositoryPath)
            .filter { it.name.endsWith(".java") }
            .map { javaTask.readInterface(it) }
            .filter { it.hasAnnotation("Repository") }
            .map { Repository(it) }
            .iterator()
    }

    /**
     * Retorna a lista de todos as classes de Rest do projeto.
     */
    val rests: Iterator<Rest> by lazy {
        val repositoryPath = path.resolve(environment.getRequiredProperty("rest.path"))
        Files.walk(repositoryPath)
            .filter { it.name.endsWith(".java") }
            .map { javaTask.readClass(it) }
            .filter { it.hasAnnotation("RestController") }
            .map { Rest(it) }
            .iterator()
    }
}
