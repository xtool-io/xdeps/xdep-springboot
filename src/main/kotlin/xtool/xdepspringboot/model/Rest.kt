package xtool.xdepspringboot.model

import org.jboss.forge.roaster.model.source.JavaClassSource

/**
 * Classe que representa uma classe Rest.
 */
data class Rest(private val javaClassSource: JavaClassSource) : JavaClassSource by javaClassSource {

    /**
     * URL com o caminho da API base da classe Rest.
     */
    val apiPath: String by lazy {
        javaClassSource.getAnnotation("RequestMapping").stringValue
    }

}
