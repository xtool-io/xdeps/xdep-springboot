package xtool.xdepspringboot.model

import org.jboss.forge.roaster.model.source.JavaClassSource
import strman.Strman
import xtool.xdepspringboot.tasks.SpringBootTask

/**
 *
 */
data class Entity(
    private val javaClassSource: JavaClassSource,
    private val springBootTask: SpringBootTask,
) : JavaClassSource by javaClassSource {

    /**
     * Retorna o nome da classe como um recurso.
     */
    val resourceName: String
        get() = Strman.toKebabCase(this.name)


    val attributes: List<EntityAttribute> by lazy {
        this.javaClassSource.fields.asSequence()
            .filter {
                listOf(
                    "Long",
                    "String",
                    "Integer",
                    "Boolean",
                    "LocalDate",
                    "LocalDateTime",
                    "BigDecimal"
                ).contains(it.type.name)
            }
            .map { EntityAttribute(it) }
            .toList()
    }

    val isReadOnly: Boolean
        get() = this.javaClassSource.hasAnnotation("Subselect") || this.javaClassSource.hasAnnotation("Immutable")

    /**
     * Retorna todas as entidades que são relacionamentos ToOne.
     */
    val toOneEntities: List<Entity> by lazy {
        this.fields.asSequence()
            .filter { it.hasAnnotation("ManyToOne") || it.hasAnnotation("OneToOne") }
            .mapNotNull { f -> springBootTask.getProject().entities.asSequence().find { it.name == f.type.name } }
            .toList()
    }

    /**
     * Retorna todas as entidades que são relacionamentos ToMany.
     */
    val toManyEntities: List<Entity> by lazy {
        this.fields.asSequence()
            .filter { it.hasAnnotation("OneToMany") || it.hasAnnotation("ManyToMany") }
            .mapNotNull { f -> springBootTask.getProject().entities.asSequence().find { it.name == f.type.typeArguments[0].name } }
            .toList()
    }
}
