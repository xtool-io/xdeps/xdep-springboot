package xtool.xdepspringboot.model

import org.jboss.forge.roaster.model.source.JavaInterfaceSource

data class Repository(private val javaInterfaceSource: JavaInterfaceSource) : JavaInterfaceSource by javaInterfaceSource {
}
