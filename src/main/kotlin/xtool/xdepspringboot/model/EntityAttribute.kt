package xtool.xdepspringboot.model

import org.jboss.forge.roaster.model.source.FieldSource
import org.jboss.forge.roaster.model.source.JavaClassSource

data class EntityAttribute(private val fieldSource: FieldSource<JavaClassSource>) : FieldSource<JavaClassSource> by fieldSource {

    val maxLength: String?
        get() = when(this.fieldSource.getAnnotation("Column").getLiteralValue("length").isNullOrBlank()){
            true -> "255"
            else -> this.fieldSource.getAnnotation("Column").getLiteralValue("length")
        }

    val unique: Boolean?
        get() = !this.fieldSource.getAnnotation("Column").getLiteralValue("unique").isNullOrBlank()

    val nullable: Boolean?
        get() = !this.fieldSource.getAnnotation("Column").getLiteralValue("nullable").isNullOrBlank()
}
